package com.ask.dynamosample.repositories;

import com.ask.dynamosample.entities.UserDetail;
import org.socialsignin.spring.data.dynamodb.repository.EnableScan;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

@EnableScan
public interface UserDetailRepository extends CrudRepository<UserDetail, String> {

}

